in _mediump vec4 varColor;
in _mediump vec2 varTexCoord;
#ifdef USE_ENV
in _mediump vec4 varEnvColor;
in _mediump vec2 varEnvCoord;
#endif  // USE_ENV
#ifdef USE_FOG
in _mediump vec2 varFogCoord;
#endif  // USE_FOG

// textures
uniform sampler2D diffuseTexture;
#ifdef USE_ENV
uniform sampler2D envTexture;
#endif  // USE_ENV

// constants
BeginBlock(Constants)
_uniform _mediump vec3 shadowColor;
_uniform _mediump vec3 fogColor;
_uniform _mediump vec2 fogParams;
EndBlock

void main(void)
{
  vec4 diffuse = texture(diffuseTexture, varTexCoord);
  #ifdef ALPHA_TEST
  if (diffuse.a < 0.5) {
    discard;
  }
  #endif  // ALPHA_TEST
  outColor = varColor * diffuse;

  #ifdef USE_ENV
  vec4 specular = texture(envTexture, varEnvCoord);
  outColor.rgb += varEnvColor.rgb * specular.rgb;
  #endif  // USE_ENV

  #ifdef USE_FOG
  float fog = clamp(varFogCoord.x, 0.0, 1.0);
  fog = clamp(fog - varFogCoord.y, 0.0, 1.0);
  #ifdef ADDITIVE
  outColor.rgb = mix(vec3(0.0), outColor.rgb, fog);
  #else
  outColor.rgb = mix(fogColor, outColor.rgb, fog);
  #endif  // ADDITIVE
  #endif  // USE_FOG
}
